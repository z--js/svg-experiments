//Note: Use thefilesaver (https://github.com/eligrey/FileSaver.js/) for downloading image
//Already included
//saveAs(blob, "image.png");


//Svg matrix utility functions

//Multiplies two svg matrices
const multiplyMatrix = (m1, m2) => {
    return [
        m1[0] * m2[0] + m1[2] * m2[1],
        m1[1] * m2[0] + m1[3] * m2[1],
        m1[0] * m2[2] + m1[2] * m2[3],
        m1[1] * m2[2] + m1[3] * m2[3],
        m1[0] * m2[4] + m1[2] * m2[5] + m1[4],
        m1[1] * m2[4] + m1[3] * m2[5] + m1[5]
    ];
}

/*
Parses svg transform attribute into translation, rotation (degrees) and scale from the given input

input :
    transformStr : string 'matrix(a,b,c,d,e,f)'
return value:
    {
        translation : [2],
        rotation : float,
        scale : [2]
    }

In translation and scale [0] is x and [1] is y coordinate
rotation is in degrees.
*/
const parseSvgMatrix = (transformStr) => {
    const index = transformStr.indexOf('(');
    const values = transformStr.substring(index + 1, transformStr.length - 1).
        split(/\,|\s/).
        map((str) => {
            return parseFloat(str);
        });

    return {
        translation: [values[4], values[5]],
        rotation: Math.atan2(values[1], values[0]) * (180 / Math.PI),
        scale: [Math.sqrt(values[0] * values[0] + values[1] * values[1]), Math.sqrt(values[2] * values[2] + values[3] * values[3])]
    }
}

/*
Creates svg transform attribute string from the given transform

input :
    {
        translation : [2],
        rotation : float,
        scale : [2]
    }
return value:
    string : 'matrix(a,b,c,d,e,f)'
*/
const createSvgMatrix = (transform) => {

    const transMatrix = [
        1, 0, 0, 1, transform.translation[0], transform.translation[1]
    ];

    const rotRadians = transform.rotation * (Math.PI / 180);
    const rotMatrix = [
        Math.cos(rotRadians), Math.sin(rotRadians), -Math.sin(rotRadians), Math.cos(rotRadians), 0, 0
    ];

    const scaleMatrix = [
        transform.scale[0], 0, 0, transform.scale[1], 0, 0
    ];

    transformMatrix = multiplyMatrix(transMatrix, multiplyMatrix(rotMatrix, scaleMatrix));
    return "matrix(" + transformMatrix.join() + ")";
};

// ^ code before this line was provided to me
// this is the code I wrote:

const hideUndefinedSpaces = (element) => {
    const undefinedSpaces = element.firstElementChild.querySelectorAll('.Undefined');

    undefinedSpaces.forEach((node) => {
        node.setAttribute('style', 'display: none')
    });
}

const setCartoonWalls = (e, elementId = 'svgHolder', outlineColour = '#646464') => {
    const svgEl = document.getElementById(elementId).firstElementChild;
    const walls = svgEl.firstElementChild.querySelectorAll('.Wall>polygon');

    walls.forEach((node) => {
        if (e.target.checked) {
            node.setAttribute('style', `stroke-width:4px;stroke:${outlineColour};stroke-linejoin:round`);
        }
        else {
            node.removeAttribute('style')
        }
    });
}

const addEventListeners = (element) => {
    const labels = element.querySelectorAll('.SpaceDimensionsLabel');

    labels.forEach((node) => {
        node.addEventListener(
            'contextmenu',
            e => {
                e.preventDefault();
            }
        );
        node.addEventListener(
            'mousedown',
            e => {
                let clockDir = 0;
                switch (e.button) {
                    case 0: // LMB
                        clockDir = -1;
                        break;
                    case 2: // RMB
                        clockDir = 1;
                }

                if (clockDir !== 0) {
                    addRotation(node, clockDir * (e.shiftKey ? 90 : 45));
                }
            }
        );
    });
}

const dumpSvgIntoDOM = (fileUrl, elementId = 'svgHolder') => {

    const container = document.getElementById(elementId);

    const setInnerHTML = (inlineSVG) => container.innerHTML = inlineSVG;

    fetch(fileUrl)
       .then( r => r.text() )
       .then( inlineSvg => setInnerHTML(inlineSvg) )
       .then( _ => hideUndefinedSpaces(container))
       .then(_ => addEventListeners(container))

}

const addRotation = (element, deg) => {
    const transformMatrixValue = element.getAttribute('transform');
    const transformation = parseSvgMatrix(transformMatrixValue);

    let viewBox = element.getAttribute('viewBox');
    if (!!!viewBox) { // not defined
        const defaultBBox = element.getBBox();

        element.setAttribute('transform-origin', `0 ${defaultBBox.height / 4}px`);
    }

    // console.log(transformation.translation)
    // console.log(transformation.rotation)

    transformation.rotation = transformation.rotation + deg;

    element.setAttribute('transform', createSvgMatrix(transformation));
}

const getSvgRatio = (elementId = 'svgHolder') => {
    const svgEl = document.getElementById(elementId).firstElementChild;
    return svgEl.getAttribute('width') / svgEl.getAttribute('height');
}

const exportImage = (elementId = 'svgHolder') => {
    const canvasWidth = document.getElementById('resolutionX').value;
    const canvasHeight = document.getElementById('resolutionY').value;

    if (!canvasWidth || !canvasHeight) {
        alert('Please set dimensions!');
        return false;
    }

    const inlineSvg = document.getElementById(elementId).innerHTML;
    const blob = new Blob([inlineSvg], { type: "image/svg+xml" });
    // const url = URL.createObjectURL(blob);
    // saveAs(blob, 'image.svg');

    var canvas = document.createElement("canvas");
    var context = canvas.getContext("2d");

    canvas.width = canvasWidth;
    canvas.height = canvasHeight;

    let image = new Image; // create <img> element
    image.onload = function () {
        context.fillStyle = '#fff';
        context.fillRect(0, 0, canvas.width, canvas.height);

        const aspectRatio = getSvgRatio();

        let goodWidth = canvasWidth;
        let goodHeight = canvasWidth / aspectRatio;

        if (goodHeight > canvasHeight) {
            goodHeight = canvasHeight;
            goodWidth = canvasHeight * aspectRatio;
        }

        context.drawImage(image, 0, 0, goodWidth, goodHeight);

        saveAs(canvas.toDataURL("image/png"), "image.png");
    }.bind(this);

    // btoa — binary string to ASCII (Base64-encoded)
    image.src = 'data:image/svg+xml;base64,' + btoa(inlineSvg);

}

// this happens on window's load
window.addEventListener(
    'load',
    () => {
        dumpSvgIntoDOM('floorplan.svg');

        document.getElementsByTagName('button')[0].addEventListener(
            'click',
            _ => { exportImage() }
        )

        document.getElementById('cartoonWalls').addEventListener(
            'change',
            e => {
                setCartoonWalls(e);
            }
        )
    },
    {
        once: true
    }
)
